package page

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable
import object.LoginObject

public class LoginPage {

	LoginObject login = new LoginObject()

	@Keyword
	public void LoginStart(Email, Password) {
		WebUI.openBrowser(GlobalVariable.LINK_WEB)
		WebUI.maximizeWindow()
		if(WebUI.waitForElementPresent(login.englishRefresh(), 3)) {
			WebUI.comment('English')
			WebUI.refresh()
		} else {
			WebUI.comment('Indonesia')
		}
		WebUI.click(login.iconMasuk())
		WebUI.delay(3)
		WebUI.refresh()
		WebUI.setText(login.inputEmail(), Email)
		WebUI.setText(login.inputPassword(), Password)
		WebUI.click(login.btnMasuk())
	}

	@Keyword
	public void LoginCondition(Message, Negative_Case) {
		if(Negative_Case == 'Yes') {
			WebUI.verifyElementVisible(login.txtMessage(Message))
		} else {
			WebUI.comment('Positive Case')
		}
		WebUI.delay(3)
	}

	@Keyword
	public void Logout(Logout) {
		if(Logout == 'Yes') {
			WebUI.click(login.iconProfile())
			WebUI.click(login.btnLogout())
			WebUI.comment('Logout')
		} else {
			WebUI.comment('Tidak Logout')
		}
		WebUI.delay(3)
	}
}
