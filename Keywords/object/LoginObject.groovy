package object

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable
import pageobjects.BasePageObject

public class LoginObject extends BasePageObject {

	private TestObject englishRefresh
	private TestObject iconMasuk
	private TestObject inputEmail
	private TestObject inputPassword
	private TestObject btnMasuk
	private TestObject iconProfile
	private TestObject btnLogout
	private TestObject txtCondition
	private TestObject btnCloseAlert

	public TestObject englishRefresh(){
		englishRefresh = createTestObjectByXpath("englishRefresh", "//*/text()[normalize-space(.)='Login']/parent::*")
	}

	public TestObject iconMasuk(){
		iconMasuk = createTestObjectByXpath("iconMasuk", "//*/text()[normalize-space(.)='Masuk']/parent::*")
	}

	public TestObject inputEmail(){
		inputEmail = createTestObjectByID("inputEmail", "email")
	}

	public TestObject inputPassword(){
		inputPassword = createTestObjectByID("inputPassword", "password")
	}

	public TestObject btnMasuk(){
		btnMasuk = createTestObjectByType("btnMasuk", "submit")
	}

	public TestObject iconProfile(){
		iconProfile = createTestObjectByXpath("iconProfile", "(.//*[normalize-space(text()) and normalize-space(.)='Personal Care'])[1]/preceding::span[1]")
	}

	public TestObject btnLogout(){
		btnLogout = createTestObjectByXpath("btnLogout", "//*/text()[normalize-space(.)='Keluar']/parent::*")
	}

	public TestObject txtMessage(Message){
		txtCondition = createTestObjectByXpath("txtCondition", "//*/text()[normalize-space(.)='${Message}']/parent::*")
	}

	public TestObject btnCloseAlert(){
		btnCloseAlert = createTestObjectByXpath("btnCloseAlert", "(.//*[normalize-space(text()) and normalize-space(.)='Copyright © 2022 PowerBiz'])[1]/following::*[name()='svg'][1]")
	}
}
